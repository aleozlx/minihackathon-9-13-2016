rules = [i.split('-') for i in raw_input().split()]
encrypted = raw_input()
def f(c):
    c = c.upper()
    for r in rules:
        if c == r[0]:
            return r[1].lower()
    return c.lower()
# print rules, encrypted, f('n')
r=''.join(f(c) for c in encrypted)
print r[0].upper()+r[1:]
